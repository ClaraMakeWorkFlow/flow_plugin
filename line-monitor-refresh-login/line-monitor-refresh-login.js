// easy copy for installing flow.plugin powerapp: https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/line-monitor-refresh-login/line-monitor-refresh-login.js
// ==UserScript==
// @name         LineMonitor.Plugin - Auto Refresh + Login
// @description  Automatically click a button with specific classes when it appears
// @namespace    https://makeworkflow.de
// @version      1.1.2

// @match        https://apps.powerapps.com/play/*

// @copyright    MAKE WORK FLOW GmbH
// @author       Feiko Bronsveld
// @sandbox      JavaScript
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const checkInterval = 5000; // Time in milliseconds between checks for the button

    const clickButtonIfLabelIsRefreshOrSignIn = () => {
        // Find all elements that could potentially contain the text "Refresh"
        document.querySelectorAll('.ms-Button-label').forEach(label => {
            if (label.textContent.trim().toLowerCase() === "refresh") {
                console.log('Button with "Refresh" label found and clicked!');
                label.closest('.ms-Button').click(); // Click the closest button ancestor of the label
            }
        });

        // Find all button elements that could potentially have the value "Sign in"
        document.querySelectorAll('.ms-Button-label').forEach(label => {
            if (label.textContent.trim().toLowerCase() === "sign in") {
                console.log('Button with "Sign in" value found and clicked!');
                label.closest('.ms-Button').click();
            }
        });
    };

    // Start an interval that checks for and clicks the button every 5 seconds
    setInterval(clickButtonIfLabelIsRefreshOrSignIn, checkInterval);

    // Check if the page has been reloaded
    if (!localStorage.getItem('reloaded1')) {
        // Set the reloaded flag
        localStorage.setItem('reloaded1', 'true');

        // Set a timer to reload the page after 10 seconds
        setTimeout(function() {
            window.location.reload();
        }, 10000);
    } else {
        // Remove the reloaded flag after reloading
        localStorage.removeItem('reloaded1');
    }

})();