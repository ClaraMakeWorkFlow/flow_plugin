function setPlanningFLowTrafficLight(target, trafficLightSettings) {
        let text = target.textContent.trim();;
        let percentage = parseInt(text, 10);
        let trafficLightColor = "";

        if (!isNaN(percentage)) {
            // If there was no percentage sign, append it
            text = text.endsWith("%") ? text : `${percentage}%`;

            if (percentage >= trafficLightSettings.green) {
                trafficLightColor = " 🟢"; // Green circle for 100%
            } else if (percentage >= trafficLightSettings.orange) {
                trafficLightColor = " 🟡"; // Orange circle for over and 50%
            } else if (percentage < trafficLightSettings.red) {
                trafficLightColor = " 🔴"; // Red circle for under 50%
            }
            // Append the appropriate emoji to the text
            if (!text.includes(trafficLightColor)) {
                target.textContent = text + trafficLightColor;
            }
        }
    }

// LOGIC TO ADD PLANNINGFLOW TRAFFIC LIGHTS TO SPECIFIC FIELDS
function planningFlowLogic(target, planningFlowFields, trafficLightSettings){
    if (planningFlowFields.includes(target.textContent)) {
        // get and set the parent of the target
        const parentElement = target.parentNode;
        // get and set the value of the field
        let nextEditableField = parentElement.querySelector('.value .text-ellipsis');

        // Configuration of the observer:
        const config = {childList: true, characterData: true, subtree: true };

        // Create an observer to watch for changes on parent level
        const observer = new MutationObserver(function(mutations) {
            const mutation = mutations[mutations.length - 1];
            if (mutation.type === "characterData" || mutation.type === "childList") {
                //get correct field value parent
                let mutationEditableField = "";
                if (mutation.target.nodeType === Node.ELEMENT_NODE) {
                    mutationEditableField = mutation.target.querySelector('.value .text-ellipsis');
                } else {
                    mutationEditableField = mutation.target.parentElement
                }

                if (mutationEditableField !== null) {
                    // disconnect to prevent infinite loop
                    observer.disconnect();

                    // update field with correct emoji
                    setPlanningFLowTrafficLight(mutationEditableField, trafficLightSettings);

                    // Reconnect the observer after the update
                    observer.observe(parentElement, config);
                }
            }
        });

        // on first load add trafficlights
        if (parentElement && nextEditableField) {
            // set traffic light on first page load
            setPlanningFLowTrafficLight(nextEditableField, trafficLightSettings);
            // Start observing the target node for configured mutations, always after first trafficlight set
            observer.observe(parentElement, config);
        } else {
            console.error('Target node not found');
        }
    }
}
