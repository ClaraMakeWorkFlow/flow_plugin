function translateTileFields(labelDiv, translationFields) {
    // Ensure label is a string
    if (typeof labelDiv.textContent === "string") {
        // Convert the label's text content to uppercase for direct comparison
        const labelText = labelDiv.textContent.toUpperCase();
        // Check if the text content directly matches any key in the translation dictionary
        if (translationFields[labelText]) {
            // Replace the label's text content with the translation
            labelDiv.textContent = translationFields[labelText];
        }
    }
};
