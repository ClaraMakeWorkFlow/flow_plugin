function setFlagPoleToSwimlaneColor(){
    const headerElements = document.querySelectorAll('.flex-column.kanban-board-row.expanded');
    const flagCardColors = document.querySelectorAll('div.card-flag');

    if (headerElements.length > 0 && flagCardColors.length > 0 ) {
        // set all colors correct
        flagCardColors.forEach(flagCardColor => {
            // Target element found
            const headerPath = flagCardColor.parentNode.parentNode.parentNode.previousElementSibling
            // only set color if exists
            if (headerPath) {
                const headerColor = getComputedStyle(headerPath).backgroundColor;
                flagCardColor.style.backgroundColor = headerColor;
            }
        });

        // Create a new MutationObserver
        headerElements.forEach(targetElement => {
            const observer = new MutationObserver(function(mutationsList) {
                for (let mutation of mutationsList) {
                    if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                        const filteredNodes = Array.from(mutation.addedNodes).filter(node => node.nodeType === Node.ELEMENT_NODE && node.classList.contains('wit-card'));
                        if (filteredNodes.length > 0) {
                            handleChangedSwimlane(filteredNodes);
                        }
                    }
                }
            });

            // Configure and start the observer
            const observerConfig = { childList: true, subtree: true };
            observer.observe(targetElement, observerConfig);

            // Handle child added event
            function handleChangedSwimlane(addedNodes) {
                addedNodes.forEach(function(node) {
                    if (node.nodeType === Node.ELEMENT_NODE) {
                        const headerColor = getComputedStyle(node.firstElementChild.parentNode.parentNode.parentNode.previousElementSibling).backgroundColor;
                        node.firstElementChild.style.backgroundColor = headerColor;
                    }
                });
            };
        });
    } else {
        // Target element not found, retry after a delay
        setTimeout(setFlagPoleToSwimlaneColor, 250);
    }
}
