/////// RAW LINK FOR DIRECT COPY: https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dcm-kids-sgb-140-de/dcm-kids-sgb-140-de.js

// ==UserScript==
// @name         Flow.Plugin - DCM Kids SGB 140 - DE
// @description  FLOW.PLUGIN to have custom features on the MS DEVOPS board for increased visability and usability
// @namespace    https://makeworkflow.de
// @run-at       document-end
// @grant        unsafeWindow
// @version      0.0.2

// @match        https://dev.azure.com/Kids-SGB-140/SGB%20140/
// @match        https://dev.azure.com/Kids-SGB-140/SGB%20140/_boards/board/t/*

// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/jquery.min.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/waitForKeyElements.js

// @copyright    MAKE WORK FLOW GmbH
// @author       Feiko Bronsveld

// ==/UserScript==

(function() {
    'use strict';

    // ON IMAGE FIELD FOUND
    let imageFieldLabel = 'div.label.text-ellipsis:contains("IMAGE URL")';

    // ON IMAGE UPDATE
    let onOpenItemImageFieldElement = 'input#__bolt-IMAGE-URL-input';
    let onOpenItemWorkItemIDElement = 'div.flex-row.body-xl.padding-bottom-4.padding-right-8.flex-center';

    // SET DATES TO LOCALE
    let onOpenItemDates = 'time.bolt-time-item.white-space-nowrap';

    // SET FIELDS ON CARD DIV
    let fieldsOnCardDiv = 'div.fields';

    // SET BOARD COLUMN DOING / DONE
    let boardColumnSplitElement = "div.kanban-board-column";

    // WORDS TO TRANSLATE
    // const translationDictionary = {
    //     "COLOR": "颜色||COLOR"
    // };

    // ExtensionCache DB setup
    const ongoingCaching = new Map();
    const blobURLCache = new Map();
    const dbName = "ExtensionCacheDB";
    const storeName = "images";
    let db;

    // OPEN DB CONNECTION
    const openDB = () => {
        return new Promise((resolve, reject) => {
            const request = indexedDB.open(dbName, 1);

            request.onupgradeneeded = function(event) {
                db = event.target.result;
                if (!db.objectStoreNames.contains(storeName)) {
                    db.createObjectStore(storeName, { keyPath: "url" });
                }
            };

            request.onsuccess = function(event) {
                db = event.target.result;
                resolve(db);
            };

            request.onerror = function(event) {
                console.error("IndexedDB error:", event.target.errorCode);
                reject(event.target.errorCode);
            };
        });
    };

    // Initialize IndexedDB
    openDB();

    // CACHE IMAGE
    const cacheImage = (url, callback) => {
        fetch(url)
            .then(response => {
            if (!response.ok) {
                throw new Error(`Network response was not ok, status: ${response.status}`);
            }

            // Check if the content type of the response is suitable for blob conversion
            const contentType = response.headers.get("content-type");
            if (!contentType || !contentType.includes("image")) {
                throw new Error(`Response is not an image, content type: ${contentType}`);
            }

            return response.blob();
        })
            .then(blob => {
            // Blob conversion should be successful here
            const transaction = db.transaction([storeName], "readwrite");
            const store = transaction.objectStore(storeName);
            var putRequest = store.put({ url: url, data: blob });

            putRequest.onsuccess = function() {
                console.log("Image successfully cached in IndexDB and saved to blob url cache");
                const URLObject = window.URL || window.webkitURL;
                const imageURL = URLObject.createObjectURL(blob);
                blobURLCache.set(url, imageURL);

                if (typeof callback === "function") {
                    callback(imageURL);
                }
            };

            putRequest.onerror = function(event) {
                console.log("Error caching image:", event.target.error);
            };
        })
            .catch(error => console.error('Error during fetching and caching:', error));
    };

    const retrieveImage = (url, callback) => {
        if (ongoingCaching.has(url)) {
            ongoingCaching.get(url).then(() => {
                if (blobURLCache.has(url)) {
                    console.log("Image found in blob cache, using blob url cache");
                    callback(blobURLCache.get(url));
                }
            });
            return;
        }

        // Create a promise representing the caching operation
        const cachingPromise = new Promise((resolve, reject) => {
            const transaction = db.transaction([storeName], "readonly");
            const store = transaction.objectStore(storeName);
            const request = store.get(url);

            request.onsuccess = function(event) {
                if (event.target.result) {
                    console.log("Image found in IndexedDB cache, converted to blob and saved in blob url cache");
                    const URLObject = window.URL || window.webkitURL;
                    const imageURL = URLObject.createObjectURL(event.target.result.data);
                    blobURLCache.set(url, imageURL);
                    callback(imageURL);
                    resolve();
                } else {
                    console.log("Image not found in indexedDB cache, caching now.");
                    cacheImage(url, (cachedSrc) => {
                        callback(cachedSrc);
                        resolve();
                    });
                }
            };

            request.onerror = function(event) {
                console.error('Error in retrieving from indexedDB cache:', event);
                reject();
            };
        });

        // Add the caching operation to the map
        ongoingCaching.set(url, cachingPromise);

        // Once caching is complete, remove it from the map
        cachingPromise.finally(() => {
            ongoingCaching.delete(url);
        });
    };

    // TEST IF URL IS VALID IMAGE
    function testImageUrl(url) {
        return new Promise(function(resolve, reject) {
            var image = new Image();

            image.onload = function() {
                resolve(true);
            };

            image.onerror = function() {
                resolve(false);
            };

            image.src = url;
        });
    }

    function updateImageElement(imageElement, src, jNode) {
        if (!imageElement.length) {
            let img = document.createElement("img");
            img.src = src;
            img.width = 84;
            img.height = 55;
            img.style.marginRight = "auto";
            img.className = "workItemPictures";
            jNode.parent().parent().prev().children(':first').prepend(img);
        } else {
            imageElement.attr('src', src);
        }
        jNode.parent().hide();
    }

    // ON IMAGE FIELD FOUND
    function onImageFieldFound(jNode) {
        var imageURL = jNode.next().text();
        var image = jNode.parent().parent().find("img");

        if (!imageURL || !testImageUrl(imageURL)) {
            console.log("Invalid or empty IMAGE URL, skipping fetch and cache.");
            image.remove();
            return;
        }

        retrieveImage(imageURL, function(cachedSrc) {
            if (cachedSrc) {
                updateImageElement(image, cachedSrc, jNode);
            } else {
                cacheImage(imageURL, function(blob) {
                    const URLObject = window.URL || window.webkitURL;
                    const imageURL = URLObject.createObjectURL(blob);
                    updateImageElement(image, imageURL, jNode);
                });
            }
        });
    }
    waitForKeyElements(imageFieldLabel, onImageFieldFound);

    // ON IMAGE FIELD UPDATE
    function onImageFieldUpdate(jNode) {
        // FORCE UPDATE BEFORE SHOWING FIELDS
        autoUpdate();

        // get all buttons
        var buttons = $('button');
        // get initial imageURL value
        var initialImageURL = jNode.attr('value');

        //updated picture and give feedback to user
        jNode.on('change', function() {
            // GET new image url on change
            var newImageURL = $(this).val();
            // Get all images on board with the old image
            var targetImages = $(`img[src="${initialImageURL}"]`);

            // ONLY DO SOMETGING IF NEW URL AND NewImageURL not empty
            if (newImageURL !== initialImageURL && newImageURL) {

                // TEST IF IMAGE URL IS VALID IMG
                testImageUrl(newImageURL).then(function(isValidImage) {
                    if (isValidImage) {
                        buttons.prop('disabled', false);
                        jNode.css('color', 'green');
                        // console.log("URL is a valid image.");
                        // SET NEW IMAGE
                        targetImages.attr("src", newImageURL);
                    } else {
                        buttons.prop('disabled', true);
                        jNode.css('color', 'red');
                        jNode.val('Not valid image link!');
                        jNode.attr('value', 'Not a valid image linke!');
                        // console.log("URL is not a valid image.");
                        // REMOVE OLD IMAGE
                        targetImages.remove();
                    }
                });
            } else if (!newImageURL) {
                buttons.prop('disabled', false);
                targetImages.remove();
            }
        });
    }
    waitForKeyElements(onOpenItemImageFieldElement, onImageFieldUpdate);

    // TRANSLATE FIELDS ON ITEM TO CHINESE
    function fieldsOnCardFound(jNode) {
        // change fields value to right aling
        $('.value').css('text-align', 'right');
        // change edit fields value to the right align
        $('.editor-component').css('text-align', 'right');
        // change fields name to be long
        $('.label').css('overflow', 'visible');

        // Find all 'div.label.text-ellipsis' elements within jNode
        const labelDivs = jNode.find('div.label.text-ellipsis');

        // Process each labelDiv
        labelDivs.each(function() {
            // Get current text of the div
            let text = $(this).text();

            // Ensure text is a string
            if (typeof text === "string") {
                // Replace words based on the dictionary
                for (const [key, value] of Object.entries(translationDictionary)) {
                    const regex = new RegExp(`\\b${key}\\b`, 'gi');
                    text = text.replace(regex, value);
                }

                // Update the div's text content
                $(this).text(text);
            }
        });
    }
    // waitForKeyElements(fieldsOnCardDiv, fieldsOnCardFound);

    //CHANGE COLUMN SPLIT DOING / DONE TO SOMETHING ELSE :D
    function boardColumnSplitRename(kanbanBoardColumn){
        let splitColumn = kanbanBoardColumn.children(':first');
        if ( splitColumn.is("span") ){
            let textSplitColumn = splitColumn.text().trim()
            if ( textSplitColumn === 'Doing') {
                splitColumn.text("WAITING");
            } else if ( textSplitColumn === 'Done') {
                splitColumn.text("IN REVIEW");
            }
        }
    };
    waitForKeyElements(boardColumnSplitElement, boardColumnSplitRename);

    // ON OPEN ITEM CHANGE DATES
    function onOpenItemChangeDates(jNode) {
        let datetimeValue = jNode.attr('datetime');
        let dateTime = new Date(datetimeValue);
        let correctFormat = dateTime.toLocaleString();
        jNode.text(correctFormat).css('font-weight', 'bold');
    }
    waitForKeyElements(onOpenItemDates, onOpenItemChangeDates);

    function autoUpdate(){
        // check if image exists and image is there, remove image
        var images = $('img.workItemPictures');

        images.each(function() {
            var image = $(this);
            var imageField = image.parent().parent().parent().find(imageFieldLabel);
            var imageURL = imageField.next().text();

            // if imagefield is empty remove old picture
            if (imageField.length === 0) {
                image.remove();
            } else if (image.attr('src') != imageURL) {
                // if image url and image have different URL update it
                image.attr("src", imageURL);
            }
        });
    }

    function setChildrenColor(){
        const headerElements = document.querySelectorAll('.flex-column.kanban-board-row.expanded');
        const flagCardColors = document.querySelectorAll('div.card-flag');

        if (headerElements.length > 0 && flagCardColors.length > 0 ) {
            if (doOnce){
                // set all colors correct ONCE
                flagCardColors.forEach(flagCardColor => {
                    // Target element found
                    const headerPath = flagCardColor.parentNode.parentNode.parentNode.previousElementSibling
                    // only set color if exists
                    if (headerPath) {
                        const headerColor = getComputedStyle(headerPath).backgroundColor;
                        flagCardColor.style.backgroundColor = headerColor;
                    }
                });
                doOnce = false;
            }

            // Create a new MutationObserver
            headerElements.forEach(targetElement => {
                const observer = new MutationObserver(function(mutationsList) {
                    for (let mutation of mutationsList) {
                        if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                            const filteredNodes = Array.from(mutation.addedNodes).filter(node => node.classList.contains('wit-card'));
                            if (filteredNodes.length > 0) {
                                handleChildAdded(filteredNodes);
                            }
                        }
                    }
                });

                // Configure and start the observer
                const observerConfig = {
                    childList: true,
                    subtree: true
                };
                observer.observe(targetElement, observerConfig);

                // Handle child added event
                function handleChildAdded(addedNodes) {
                    addedNodes.forEach(function(node) {
                        if (node instanceof HTMLElement) {
                            const headerColor = getComputedStyle(node.firstElementChild.parentNode.parentNode.parentNode.previousElementSibling).backgroundColor;
                            node.firstElementChild.style.backgroundColor = headerColor;
                        }
                    });
                };
            });
        } else {
            // Target element not found, retry after a delay
            setTimeout(setChildrenColor, 250);
        }
    }

    // Reload page every 10 minutes
    setInterval(function() {
        console.log("Reload whole page");
        location.reload()
    }, 10 * 60 * 1000);

    let doOnce = true;
    setChildrenColor();
})();
